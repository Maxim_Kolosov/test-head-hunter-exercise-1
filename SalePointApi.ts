import SalePoint from '@/models/api/SalePoint';
import {
    createResource,
    deleteResource,
    getPaginatedResourceList,
    getResource,
    getResourceList,
    updateResource
} from '@/services/api/utils';
import PaginationParams from '@/models/util/PaginationParams';
import PaginationResult from '@/models/util/PaginationResult';
import ModelPermission from '@/models/api/permissions/ModelPermission';

export default class SalePointApi {
    public getListPaginated(paginationParams: PaginationParams): Promise<PaginationResult<SalePoint>> {
        return getPaginatedResourceList('objects/', SalePoint, paginationParams);
    }

    public getListByCompany(id: number, withDevicesOnly: boolean = false): Promise<SalePoint[]> {
        return getResourceList(`objects/?company=${id}&contains_devices=${withDevicesOnly}`, SalePoint);
    }

    public getListByUser(id: number): Promise<SalePoint[]> {
        return getResourceList(`users/${id}/company_objects/`, SalePoint);
    }

    public getListByBrand(id: number): Promise<SalePoint[]> {
        return getResourceList(`objects/?brand=${id}`, SalePoint);
    }

    public getList(urlParams?: {}): Promise<SalePoint[]> {
        return getResourceList('objects/', SalePoint, urlParams);
    }

    public getNavigationUrl(id: number): Promise<SalePoint> {
        return getResource(`objects/${id}/get_navigation_url/`, Object);
    }

    public get(id: number, urlParams?: {}): Promise<SalePoint> {
        return getResource(`objects/${id}`, SalePoint, urlParams);
    }

    public update(id: number, data: object): Promise<any> {
        return updateResource(`objects/${id}/`, data)
    }

    public create(data: object): Promise<any> {
        return createResource('objects/', data)
    }

    public delete(id: number): Promise<any> {
        return deleteResource(`objects/${id}/`);
    }

    public getPermissions(): Promise<ModelPermission> {
        return new Promise<ModelPermission>(async (resolve, reject) => {
            try {
                const permissions = await getResource('objects/permissions/', ModelPermission);
                permissions.fieldsMapping = {
                    address: 'address_text',
                    name: 'name_text'
                };

                resolve(permissions);
            } catch (e) {
                reject(e);
            }
        });
    }
}
